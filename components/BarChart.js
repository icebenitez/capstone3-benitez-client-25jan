import {useState, useEffect} from 'react'
import {Bar} from 'react-chartjs-2'
import moment from 'moment'


export default function BarChart({data}){

	const months = ['January','February','March','April','May','June','July','August','September','October','November','December']
	const [monthlyData, setMonthlyData] = useState([])

	//console.log(data)

	/*useEffect(() => {
		let tempMonths = [] 
		if(data.length > 0){
			data.forEach(element => {
				if(!tempMonths.find(month => month === moment(element.sale_date).format('MMMM'))){
					tempMonths.push(moment(element.sale_date).format('MMMM'))
				}
			})
			
		}
		setMonths(tempMonths)
	},[data])*/

	//console.log(months)

	useEffect(()=>{

		//Monthly expense will then be populated by the accumulated expense for the specific month.
		setMonthlyData(months.map(month => {

			let amount = 0

			data.forEach(element => {

				//console.log(moment(element.sale_date).format("MMMM"))
				if(moment(element.dateMade).format('MMMM') === month){

					amount = amount + parseInt(element.amount)

				} else {
					return 0
				}

			})

			return amount

		}))
		

	},[data])

	//console.log(monthlyData)

	return(

			<Bar data = {{

				datasets:[{

					data: monthlyData,//array of numbers
					backgroundColor: ["lightblue","black","green","yellow"]
				
				}],
				labels: months
			}} />
		)
}