import {useContext} from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Jumbotron from 'react-bootstrap/Jumbotron'
import Link from 'next/link'

import UserContext from '../UserContext'


export default function NavBar(){

	const {user} = useContext(UserContext)

	//console.log(user)

	return (
			
			<Navbar bg="dark" variant="dark" expand="lg">
						<Link href="/">
							<a className="navbar-brand">Budget Friendly</a>
						</Link>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
						<Nav className="mr-auto">
						{
							user.email
							?
							<>
							<Link href="/categories">
							<a className="nav-link" role="button">Categories</a>
							</Link>

							<Link href="/records">
							<a className="nav-link" role="button">Records</a>
							</Link>

							<Link href="/charts/monthly-income">
							<a className="nav-link" role="button">Monthly Income</a>
							</Link>

							<Link href="/charts/monthly-expense">
							<a className="nav-link" role="button">Monthly Expense</a>
							</Link>

							<Link href="/charts/balance-trend">
							<a className="nav-link" role="button">Trend</a>
							</Link>

							<Link href="/charts/category-breakdown">
							<a className="nav-link" role="button">Breakdown</a>
							</Link>

							<Link href="/logout">
							<a className="nav-link" role="button">Log out</a>
							</Link>	
							</>						
							:
							<>
							<Link href="/register">
							<a className="nav-link" role="button">Register</a>
							</Link>	

							<Link href="/login">
							<a className="nav-link" role="button">Log in</a>
							</Link>	
							</>	

						}
							
							

						</Nav>
				</Navbar.Collapse>
			</Navbar>	


		)

}