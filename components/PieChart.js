import {useState, useEffect} from 'react'
import {Pie} from 'react-chartjs-2'

export default function PieBreakdown({data}){
	//console.log(data)
	
	const [names, setNames] = useState([])
	const [dataPerCategories, setDataPerCategories] = useState([])

	//this will get all the names of categories made by user once data is received
	useEffect(()=>{
		if(data.length > 0){
			//console.log(data)
			let tempCategories = [] 
			data.forEach(element => {
				//console.log(element)
				if(!tempCategories.find(category => category === element.categoryName)){
					tempCategories.push(element.categoryName)
				}
			})
			setNames(tempCategories)
		}

	},[data])

	
	//this will add all the records that fall within the same categories.
	useEffect(()=>{

		setDataPerCategories(names.map(name => {

			let total = 0

			data.forEach(element => {
				if(element.categoryName === name){
					total = total + element.amount
				}
			})
			console.log(total)
			return total
		}))
	},[names])

	return(

			<Pie data = {{

				labels: names,
				datasets:[{

					data: dataPerCategories,//array of numbers
					backgroundColor: ["green","lightblue","red"]
				
				}]

			}} />

		)

}
