import {useState, useEffect} from 'react'

import Card from 'react-bootstrap/Card'
import {Row, Col} from 'react-bootstrap'
import {Fragment} from 'react'

export default function Record({prop, date}){

	const {_id, description, categoryType, categoryName, amount, balance} = prop
	
	return(
		<Fragment>
			<Card className="mb-3">
				<Card.Body>
					<Row>
						<Col md={6}>
							<h5>{description}</h5>
							{
								categoryType === "Expense"
								?
								<h6><span className="text-danger">{categoryType}</span> ({categoryName})</h6>
								:
								<h6><span className="text-success">{categoryType}</span> ({categoryName})</h6>

							}
							<p>{date}</p>
						</Col>
						<Col md={6} className="text-right">
							{
								categoryType === "Expense"
								?
								<>
								<h6 className="text-danger">- {amount}</h6>
								<span className="text-danger">{balance}</span>
								</>							
								:
								<>
								<h6 className="text-success">+ {amount}</h6>
								<span className="text-success">{balance}</span>
								</>
							}
							{}
						</Col>
					</Row>
				</Card.Body>
				
			</Card>
		</Fragment>


		)

}