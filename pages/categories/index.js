import {useContext,useEffect,useState,Fragment} from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Table from 'react-bootstrap/Table'
import AppHelper from '../../app_helper.js'


export default function Categories() {

  const [categoriesRows, setCategoriesRows] = useState([])

  useEffect(() => {

    const options = {
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`
      }
    }

    fetch(`${AppHelper.API_URL}/users/categories`, options)
    .then(AppHelper.toJSON)
    .then(data => {
      console.log(data)
      const categoriesAll = data.map(category => {

        return (
          <tr key ={category._id}>
            <td>{category.categoryName}</td>
            <td>{category.categoryType}</td>
          </tr> 

          )
      })
      setCategoriesRows(categoriesAll)
    })
  },[])

  return (

      <div className="pt-3 mb-5 container">
      <Fragment>
      <h3>Categories</h3>
        <a className="btn btn-success mt-1 mb-3" href="/categories/new">Add</a>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Category</th>
            <th>Type</th>
          </tr>
        </thead>
        <tbody>
          {categoriesRows}
        </tbody>
      </Table>
      </Fragment>
      </div>

    )
}
