import {Fragment,useState} from 'react'
import {Row,Col} from 'react-bootstrap'
import Form from 'react-bootstrap/Form'
import LineChartTrend from './../../components/LineChart'

export default function BalanceTrend(){

	const [dateFrom,setDateFrom] = useState('')
	const [dateTo,setDateTo] = useState('')

	return(


			<Fragment>
			<h2>Balance Trend</h2>

		
			<Form>
				<Row>
					<Col>				
						<Form.Group>
							<Form.Label>From</Form.Label>
							<Form.Control type="date" className="form-control" value={dateFrom} placeholder="2020-12-22" />
						</Form.Group>
					</Col>
					<Col>
						<Form.Group>
							<Form.Label>To</Form.Label>
							<Form.Control type="date" className="form-control" value={dateTo} placeholder="2021-01-22" />
						</Form.Group>
					</Col>				
				</Row>
			</Form>			
			<hr />
			<LineChartTrend />

			</Fragment>

		)
}