import {Fragment,useState,useEffect} from 'react'
import AppHelper from '../../app_helper.js'
import PieBreakdown from './../../components/PieChart'
import {Row,Col} from 'react-bootstrap'
import {Container} from 'react-bootstrap'
import Form from 'react-bootstrap/Form'

export default function CategoryBreakdown(){

	const [dateFrom,setDateFrom] = useState('')
	const [dateTo,setDateTo] = useState('')

	const [categoryData, setCategoryData] = useState([])

	//this will fetch all the data at initial render. and store the data in a state
	useEffect(()=>{
		let tempArr = []
		const datum = {
			headers: {
				Authorization: `Bearer ${AppHelper.getAccessToken()}`
			}
		}

		fetch(`${AppHelper.API_URL}/users/records`, datum)
		.then(res => res.json())
		.then(data => {
			//console.log(data)			
			data.map(record => {
				tempArr.push(record)
			})
			//console.log(tempArr)
			setCategoryData(tempArr)
		})
	},[])

	return(
			<div className="mt-5 pt-4 mb-5">
			<Container>
			<Fragment>
			<h2 className="text-center">Category Breakdown</h2>
		
			<Form>
				<Row>
					<Col>				
						<Form.Group>
							<Form.Label>From</Form.Label>
							<Form.Control type="date" className="form-control" value={dateFrom} placeholder="2020-12-22" />
						</Form.Group>
					</Col>
					<Col>
						<Form.Group>
							<Form.Label>To</Form.Label>
							<Form.Control type="date" className="form-control" value={dateTo} placeholder="2021-01-22" />
						</Form.Group>
					</Col>				
				</Row>
			</Form>			
			<hr />
			<PieBreakdown data={categoryData}/>

			</Fragment>
			</Container>
			</div>
		)
}