import {useEffect,useState,useContext} from 'react'

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

import UserContext from '../../UserContext'

import users from '../../data/users'

import Router from 'next/router'

import AppHelper from '../../app_helper'

export default function Login() {

	const {user,setUser} = useContext(UserContext)

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [isActive, setIsActive] = useState(false)
	const [willRedirect,setWillRedirect] = useState(false)



useEffect(()=>{		

		if((email !== '' && password !== '')){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[email,password])

function authenticate(e){

	e.preventDefault()

	fetch('http://localhost:4000/api/users/login',{

		method: "POST",
		headers: {

			'Content-Type': 'application/json'

		},
		body: JSON.stringify({

			email: email,
			password: password

		})

	})
	.then(res => res.json())
	.then(data => {

		if(data.accessToken){
			
		localStorage.setItem('token', data.accessToken)

		fetch('http://localhost:4000/api/users/details',{

			headers: {

				Authorization: `Bearer ${data.accessToken}`

			}

		})
		.then(res => res.json())
		.then(data => {

			setUser({

				id: data._id,
				email: data.email				

			})

			Router.push('/records')

		})

		} else {

			alert("Authentication Failed")

		}

		

	})


	setEmail('')
	setPassword('')

}

	return(
				

				<Form onSubmit={(e) => authenticate(e)}>
					<Form.Group controlId="userEmail">
						<Form.Label>Enter Your Email Address</Form.Label>
						<Form.Control type="email" placeholder="Email Address" value={email} onChange={(e) => setEmail(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="password1">
						<Form.Label>Enter Your Password:</Form.Label>
						<Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} required/>
					</Form.Group>				
					{

						isActive 
						? <Button variant="primary" type="submit">Submit</Button>
						: <Button variant="primary" disabled>Submit</Button>

					}					
				</Form>

			)


}