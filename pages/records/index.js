import {useState,useEffect} from 'react'

import Record from '../../components/RecordsCard'
import AppHelper from '../../app_helper.js'

import {Fragment} from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import moment from 'moment'

export default function Records() {

  const [records, setRecords] = useState([])
  const [cateType, setCateType] = useState('All')

  useEffect(() => {
    const option = {
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`
      }
    }

    fetch(`${AppHelper.API_URL}/users/records`, option)
    .then(AppHelper.toJSON)
    .then(data => {
      data.reverse()
      const allRecords = data.map(record => {
        //console.log(record)
        const date = moment(record.dateMade).format('MMMM DD, YYYY')
          return <Record key={record._id} prop={record} date={date}/>
      })
      setRecords(allRecords)
    })
    
  },[])

  function filter(e){
    const option = {
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`
      }
    }

    if(e === "All"){
      fetch(`${AppHelper.API_URL}/users/records`, option)
      .then(AppHelper.toJSON)
      .then(data => {
        //console.log(data)

        const allRecords = data.reverse().map(record => {
          //console.log(record)
          const date = moment(record.dateMade).format('MMMM DD, YYYY')
            return <Record key={record._id} prop={record} date={date}/>
        })
        setRecords(allRecords)
      })
    } else {
      fetch(`${AppHelper.API_URL}/users/records`, option)
      .then(AppHelper.toJSON)
      .then(data => {
        //console.log(data)
        data.reverse()
        const allRecords = data.map(record => {
          //console.log(record)
          if(record.categoryType === e){
            const date = moment(record.dateMade).format('MMMM DD, YYYY')
            return <Record key={record._id} prop={record} date={date}/>
          }
        })
        setRecords(allRecords)
      })    
    }

  }


  return (

    <>
  		<div className="pt-3 mb-5 container">
  		<h3>Records</h3>
  		<Form> 		
  			
  			<Form.Group>
  			<div className="mb-2 input-group">
  				<Button href="/records/new" className="btn btn-success">Add</Button>
  				<Form.Control type="text" placeholder="Search Record"/>
  				<Form.Control as="select" onChange={e => filter(e.target.value)}>
		  			<option value="All" selected>All</option>
		  			<option value="Income">Income</option>
		  			<option value="Expense">Expense</option>
		  		</Form.Control>
				</div>
  			</Form.Group>
  			
  		</Form>
      {records}
  		</div>
    </>
  	)
}

