import {useState,useEffect,useContext,Fragment} from 'react'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Card from 'react-bootstrap/Card'
import {Container} from 'react-bootstrap'
import {Row,Col} from 'react-bootstrap'
import AppHelper from '../../app_helper.js'
import UserContext from '../../UserContext'

export default function newRecord() {

  const {user} = useContext(UserContext)

  const [categoryType,setCategoryType] = useState('')
  const [categoryName,setCategoryName] = useState('')
  const [amount,setAmount] = useState(0)
  const [description,setDescription] = useState('')
  const [showAll,setShowAll] = useState('')
  const [isActive,setIsActive] = useState(false)
  const [incomeCategories,setIncomeCategories] = useState([])
  const [expensesCategories,setExpensesCategories] = useState([])


  useEffect(()=>{

    if(categoryType !== '' && categoryName  !== '' && amount !== '' && description !== ''){

      setIsActive(true)

    } else {

      setIsActive(false)

    }

  })

  useEffect(()=>{

    const incomeAll = []
    const expenseAll = []


    //this will get fetch all the categories made on initial render.
    //the objects will then be sorted by their type.
    const info = {
      headers: {
        Authorization: `Bearer ${AppHelper.getAccessToken()}`
      }
    }

    fetch(`${AppHelper.API_URL}/users/categories`, info)
    .then(AppHelper.toJSON)
    .then(data => {
     console.log(data)

     const categoriesAll = data.map(category => {
      console.log(categoriesAll)

        if(category.categoryType === "Income"){

          incomeAll.push(category.categoryName)

        } else if(category.categoryType === "Expense"){

          expenseAll.push(category.categoryName)

        }
      

     })
     

      setIncomeCategories(
        incomeAll.map(income => (
          <option value={income}>
            {income}
          </option>
        ))
      )

      setExpensesCategories(
        expenseAll.map(expense => (
          <option value={expense}>
            {expense}
          </option>
        ))
      )

      console.log(incomeCategories)
      console.log(expensesCategories)

    })

  },[])

  //console.log(expensesCategories)


  function createRecord(e){
    //console.log(e)
    e.preventDefault()

    const payload = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${AppHelper.getAccessToken()}`
      },
      body: JSON.stringify({
        categoryType: categoryType,
        categoryName: categoryName,
        amount: amount,
        description: description
      })
    }

     fetch(`${AppHelper.API_URL}/users/add-record/`, payload)
    .then(res => res.json())
    .then(data => {
      console.log(data)
    })

    console.log(`The New Category has been added`)

    setCategoryType('')
    setCategoryName('')
    setAmount(0)
    setDescription('')
  }

  


  return (



     <Container>        
      <Row className="justify-content-center">
        <Col md={6}>
          <h3>New Record</h3>
          <Card>
            <Card.Header>Record Information</Card.Header>
            <Card.Body>
              <Form onSubmit={(e)=> createRecord(e)}>

                  <Form.Group>
                  <Form.Label>
                    Category Type:
                  </Form.Label>
                  <select 
                  value={categoryType}
                  onChange={e => setCategoryType(e.target.value)}
                  required 
                  className="form-control">
                    <option value="true" disabled="">Select Category</option>                   
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                  </select>
                </Form.Group>               

                <Form.Group>
                <Form.Label>
                    Category Name:
                </Form.Label>
                <select
                value={categoryName}
                onChange={e => setCategoryName(e.target.value)} 
                required className="form-control">
                  <option value="true" disabled="">Category Name:</option>
                   {

                    categoryType === "Income"
                    ?
                    incomeCategories
                    : 
                    categoryType === "Expense"                   
                    ?
                    expensesCategories
                    :
                    null
                    
                   }

                </select>
                </Form.Group>
          

                <Form.Group>
                  <Form.Label>
                    Amount:
                  </Form.Label>
                  <Form.Control 
                  placeholder="Enter Amount" 
                  required 
                  type="number" 
                  value={amount} 
                  onChange={e => setAmount(e.target.value)}/>
                </Form.Group>
                <Form.Group>
                  <Form.Label>
                    Description:
                  </Form.Label>
                  <Form.Control type="text" placeholder="Enter Description" value={description} onChange={e => setDescription(e.target.value)} required />
                </Form.Group>
              <Button type="submit" variant="primary">Submit</Button>
              </Form>
            </Card.Body>
          </Card>
        </Col>        
      </Row>
  </Container> 




    )



}