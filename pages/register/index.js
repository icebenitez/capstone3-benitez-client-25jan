import {useEffect,useState,useContext} from 'react'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Link from 'next/link'
import UserContext from '../../UserContext'
import Navbar from 'react-bootstrap/Navbar';
import Router from 'next/router'


export default function Register(){

	const {user} = useContext(UserContext)

	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [email, setEmail] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState("")
	const [isActive, setIsActive] = useState(false)



useEffect(()=>{	



	if((firstName !== '' && lastName !== '' &&  email !== '' && password1 !== '' && password2 !=='') && (password2 === password1)){

		setIsActive(true)

	} else {

		setIsActive(false)

	}




},[firstName,lastName,email,password1,password2])




function registerUser(e){

		e.preventDefault()

		fetch('http://localhost:4000/api/users/email-exists', {

			method: 'POST',
			headers: {								
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email

			})
			
		})
		.then(res => res.json())
		.then(data => {

			if(data === false){

				fetch('http://localhost:4000/api/users/', {

					method: "POST",
					headers: {

						"Content-Type": "application/json"

					},
					body: JSON.stringify({

						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1

					})

				})
				.then(res => res.json())
				.then(data => {
					if (data === true){

						alert("Registration Successful")
						Router.push('/login')
						

					} else {

						alert("Registration Failed")

					}

				})

			} else {

				alert("Email Already Exists.")

			}

		})

	console.log('Thank you for registering.')

	setFirstName('')
	setLastName('')
	setEmail('')
	setPassword1('')
	setPassword2('')

}
	
	return(

			<Form onSubmit={(e)=> registerUser(e)}>
				<Form.Group controlId="userFirstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="userLastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="userEmail">
					<Form.Label>Email Address</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
				</Form.Group>
				<Form.Group controlId="password1">
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="password2">
					<Form.Label>Confirm Password:</Form.Label>
					<Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
				</Form.Group>
				{

					isActive 
					? <Button variant="primary" type="submit">Submit</Button>
					: <Button variant="primary" disabled>Submit</Button>

				}
			</Form>

	
			

		)



}